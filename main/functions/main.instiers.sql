﻿-- Function: instiers(json)

DROP FUNCTION main.instiers(json);

CREATE OR REPLACE FUNCTION main.instiers(IN params json,
    OUT ikey integer)
  RETURNS SETOF integer AS
$BODY$
DECLARE
    serial_key   integer = -1::integer;
    groupname    text    = ''::text;
    currolename  text    = ''::text;
    newrolename  text    = ''::text;
    rcrd         record;

    tcivilite integer;
    tnom text;
    p_prenom text;
    p_datenaissance timestamp without time zone;
    p_commentaire text;
    p_identifiant text;
    
    p_motdepasse text;
    p_modifprivileges text;
    p_ttiers main.persontype;
    p_info_entreprise text;
    p_sex integer;
    p_opermaker integer;
    p_priority integer;
    p_opercomment integer;
    p_idtiers integer;
BEGIN
    tcivilite := (params->>'tcivilitecol')::integer; if tcivilite is null then raise exception 'Invalid parameter <tcivilitecol>'; end if;
    tnom := (params->>'tnomcol')::text; if tnom is null then raise exception 'Invalid parameter <tnomcol>'; end if;
    p_prenom := (params->>'tprenomcol')::text; if p_prenom is null then raise exception 'Invalid parameter <tprenomcol>'; end if;
    p_datenaissance := (params->>'tdatenaissancecol_qtdisplayrole')::timestamp without time zone; if p_datenaissance is null then raise exception 'Invalid parameter <tdatenaissancecol_qtdisplayrole>'; end if;
    p_commentaire := (params->>'tcommentairecol_qtdisplayrole')::text; if p_commentaire is null then raise exception 'Invalid parameter <tcommentairecol_qtdisplayrole>'; end if;
    p_identifiant := (params->>'tidentifiantcol_qtdisplayrole')::text;
    p_motdepasse := (params->>'tpwd')::text; if p_motdepasse is null then raise exception 'Invalid parameter <tpwd>'; end if;
    p_modifprivileges := (params->>'tmodifprivileges')::text; if p_modifprivileges is null then raise exception 'Invalid parameter <tmodifprivileges>'; end if;
    p_ttiers := (params->>'tttiers')::main.persontype; if p_ttiers is null then raise exception 'Invalid parameter <tttiers>'; end if;
    p_info_entreprise := (params->>'tinfoentreprisecol_qtdisplayrole')::text; if p_info_entreprise is null then raise exception 'Invalid parameter <tinfoentreprisecol_qtdisplayrole>'; end if;
    p_sex := (params->>'tsex')::integer; if p_sex is null then raise exception 'Invalid parameter <tsex>'; end if;
    p_opermaker := (params->>'topermaker')::integer; if p_opermaker is null then raise exception 'Invalid parameter <topermaker>'; end if;
    p_priority := (params->>'tprioritycomment')::integer; if p_priority is null then raise exception 'Invalid parameter <tprioritycomment>'; end if;
    p_opercomment := (params->>'tcommentairecol_qtdisplayrole')::integer; if p_opercomment is null then raise exception 'Invalid parameter <tcommentairecol_qtdisplayrole>'; end if;
    
    p_idtiers := (params->>'ikey')::integer;

    if p_idtiers is null then --INSERT
	    if p_identifiant in (select tb1.rolname from pg_catalog.pg_roles AS tb1)  then
		raise exception 'Error, the identifiant <%> already exist', p_identifiant;
	    end if;

	    if p_identifiant is not null and p_identifiant <> ''::text and p_identifiant NOT IN (SELECT tb1.rolname FROM pg_catalog.pg_roles AS tb1)  THEN
		EXECUTE 'CREATE ROLE '::text || p_identifiant || ' WITH LOGIN PASSWORD '::text || quote_literal(p_motdepasse) || ' IN ROLE '::text || p_ttiers;
	    ELSE
		raise exception 'Error, <tidentifiantcol_qtdisplayrole> can not be NULL';
	    end if;

	    insert into main.tiers(civilite_idcivilite, nom, prenom, datenaissance, commentaire, identifiant, modifprivileges, ptype, info_entreprise, sex, operatormaker, commentpriority, operatorcomment) 
	    values(tcivilite, tnom, p_prenom, p_datenaissance, p_commentaire, p_identifiant, p_modifprivileges, p_ttiers, p_info_entreprise, p_sex, p_opermaker, p_priority, p_opercomment) 
	    returning idtiers into serial_key;
    else -- UPDATE
            serial_key := p_idtiers;
	    SELECT * INTO rcrd FROM main.tiers AS tb1 WHERE tb1.idtiers = p_idtiers;
	    currolename := rcrd.identifiant;

	    IF (rcrd.identifiant IS NULL OR rcrd.identifiant = ''::text) AND p_identifiant IS NOT NULL AND p_identifiant <> ''::text THEN
		currolename := p_identifiant;
		IF currolename NOT IN (SELECT tb1.rolname FROM pg_catalog.pg_roles AS tb1)  THEN 
			EXECUTE 'CREATE ROLE '::text || currolename || ' WITH LOGIN PASSWORD '::text || quote_literal(p_motdepasse) || ' IN ROLE '::text || p_ttiers;
		ELSE
			EXECUTE 'ALTER ROLE '::text || currolename || ' WITH LOGIN PASSWORD '::text || quote_literal(p_motdepasse);
			--raise exception 'Error, the identifiant % already exist', p_identifiant;
		END IF;
	    ELSIF rcrd.identifiant IS NOT NULL AND rcrd.identifiant <> ''::text AND (p_identifiant IS NULL OR p_identifiant = ''::text) THEN        
		EXECUTE 'DROP ROLE '::text || currolename;
	    ELSIF rcrd.identifiant <> p_identifiant THEN
		newrolename := p_identifiant;

		IF newrolename NOT IN (SELECT tb1.rolname FROM pg_catalog.pg_roles AS tb1)  THEN       
		    EXECUTE 'ALTER ROLE '::text || currolename || ' RENAME TO '::text || newrolename;
		    EXECUTE 'ALTER ROLE '::text || newrolename || ' WITH LOGIN PASSWORD '::text || quote_literal(p_motdepasse);
		    currolename = newrolename;
		ELSE
		    raise exception 'Error, the identifiant % already exist', p_identifiant;
		END IF;         
	    END IF;                       

	    IF p_identifiant IS NOT NULL AND p_identifiant <> ''::text AND rcrd.ptype <> p_ttiers THEN
		EXECUTE 'REVOKE '::text || newrolename || ' FROM '::text || rcrd.ptype;
		EXECUTE 'GRANT '::text || newrolename || ' TO '::text || p_ttiers;
	    END IF;

	    IF p_identifiant IS NOT NULL AND p_identifiant <> ''::text /* AND rcrd.motdepasse <> p_motdepasse */ THEN
		EXECUTE 'ALTER ROLE '::text || currolename || ' WITH LOGIN PASSWORD '::text || quote_literal(p_motdepasse);                
	    END IF; 

	    UPDATE main.tiers AS tb1
	    SET civilite_idcivilite = tcivilite, 
	    nom                 = tnom, 
	    prenom              = p_prenom, 
	    datenaissance       = p_datenaissance, 
	    commentaire         = p_commentaire, 
	    identifiant         = p_identifiant, 
	    modifprivileges     = p_modifprivileges, 
	    ptype               = p_ttiers,
	    info_entreprise     = p_info_entreprise,
	    sex                 = p_sex,
	    operatormaker       = p_opermaker,
	    commentpriority     = p_priority,
	    operatorcomment     = p_opercomment
	    WHERE tb1.idtiers       = p_idtiers;
	end if;
    
    RETURN QUERY SELECT serial_key;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION main.instiers(json)
  OWNER TO postgres;
