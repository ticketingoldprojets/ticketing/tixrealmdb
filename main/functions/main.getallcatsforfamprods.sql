--Function: main.getallcatsforfamprods(json)

--DROP FUNCTION "main".getallcatsforfamprods(json);

CREATE OR REPLACE FUNCTION "main".getallcatsforfamprods
(
  IN  params  json
)
RETURNS TABLE 
(
  imgcol_qtdecorole  text,
  cptitre_ikey       integer,
  cptitre_cptcolor   text,
  qtdisplayrole      text,
  cptitre            text,
  cpdat              text,
  cpddvente          text,
  cpdfvente          text,
  cpdescript         text,
  cpiva              real,
  cpivalcosto        real,
  cpcosto            real,
  cpgritar           text,
  cpgritar_ikey      integer,
  cpplano_ikey       integer,
  cpaddress          integer,
  cpnoautofill       boolean,
  cpnoseasonticket   boolean,
  cpprinterid        integer,
  cpticketmodelid    integer,
  cpuserprof         integer,
  cpisaflow          boolean,
  iuid               text
)
AS
$$
DECLARE 
        /*reg record;
        msg integer;
        cmd text;*/

        srvikey integer;
BEGIN
     srvikey := (params->>'srvikey')::integer; if srvikey is null then raise exception 'Invalid parameter <srvikey>'; end if;
     
     /*FOR reg IN (SELECT * FROM main.partenaire) LOOP
         
         INSERT INTO main.queuedemessages(message)
         VALUES('<?xml version="1.0" encoding="UTF-8"?>' || xmlelement(name Command, xmlattributes('main.getallcatsforfamprods' AS "name"), xmlelement(name ItemId, xmlattributes(CAST(srvikey AS text) AS "srvikey"))))
         RETURNING id INTO msg;  
     
         cmd := '#partner=' || CAST(reg.id AS text) || ';partnerRespHandler=;partnerRespHandlerPath=;msg=' || CAST(msg AS text);     
     
         return query select cmd, 
                             null::integer, 
                             null::text, 
                             null::text, 
                             null::text, 
                             null::text, 
                             null::text,
                             null::real,
                             null::real,
                             null::real,
                             null::text, 
                             null::text, 
                             null::text, 
                             null::text, 
                             null::text;
                                 
     END LOOP;*/
     
     return query select case when c.img is null then null
                                                 else (select encode(data, 'base64') 
                                                       from main.multimedia m 
                                                       where m.id = c.img)
                         end,
                         c.id,
                         c.couleurdutitre,
                         c.libelle,
                         c.libelle,
                         to_char(c.dateseance, 'YYYY-MM-DD') || 'T' ||to_char(c.dateseance, 'HH24:MI'), 
                         to_char(c.debutvente, 'YYYY-MM-DD') || 'T' ||to_char(c.debutvente, 'HH24:MI'),
                         to_char(c.finvente, 'YYYY-MM-DD') || 'T' || to_char(c.finvente, 'HH24:MI'),
                         c.descript,
                         c.tva, 
                         c.tvasurfrais, 
                         c.frais,
                         case when c.idgrille is null then ''::text else (select nom from main.grilletarif where idtarif = c.idgrille) end,
                         c.idgrille,
                         c.idplan,
                         c.addressid,
                         c.disableautofill,
                         c.disableseasonticket,
                         c.printer,
                         (select m.idmodeldocument
                          from main.modeldocument m, main.modeldocument_par_categproduit mc
                          where mc.idcateg = c.id and mc.idmodel = m.idmodeldocument
                          limit 1),
                         c.userprof,
                         c.isaflow,
                         'categproduit'::text || c.id
                 from main.categproduit c --join main.plandesalle p on (c.idplan = p.id)
                 where c.idfamille = srvikey;
                 
END;
$$
LANGUAGE 'plpgsql'
COST 100;

ALTER FUNCTION "main".getallcatsforfamprods(json)
  OWNER TO postgres;

GRANT EXECUTE
  ON FUNCTION "main".getallcatsforfamprods(json)
TO postgres;

GRANT EXECUTE
  ON FUNCTION "main".getallcatsforfamprods(json)
TO PUBLIC;

GRANT EXECUTE
  ON FUNCTION "main".getallcatsforfamprods(json)
TO cashier WITH GRANT OPTION;