--Function: main.array_remove_item(integer[], integer)

--DROP FUNCTION "main".array_remove_item(integer[], integer);

CREATE OR REPLACE FUNCTION "main".array_remove_item
(
  IN  "array_in"  integer[],
  IN  item        integer  
)
RETURNS integer[] AS
$$
SELECT ARRAY(
  SELECT DISTINCT $1[s.i] AS "foo"
    FROM GENERATE_SERIES(ARRAY_LOWER($1,1), ARRAY_UPPER($1,1)) AS s(i)
   WHERE $2 != $1[s.i]
   ORDER BY foo
);
$$
LANGUAGE 'sql';

ALTER FUNCTION "main".array_remove_item(integer[], integer)
  OWNER TO postgres;