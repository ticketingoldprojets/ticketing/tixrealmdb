﻿-- Function: deltiers(json)

DROP FUNCTION main.deltiers(json);

CREATE OR REPLACE FUNCTION main.deltiers(params json, OUT errmsg text)
  RETURNS SETOF text AS
$BODY$
DECLARE
    error_msg   text = ''::text;
    rolename  text = ''::text;
    srvikey integer;
BEGIN  /* MADE BY CYPC */
    srvikey := (params->>'srvikey')::integer; if srvikey is null then raise exception 'Invalid parameter <srvikey>'; end if;
    
    -- seguramente faltan otras eliminaciones en cascada (puede ser con triggers)
    -- DELETE FROM main.tiers_has_coordonnee AS tb1 WHERE tb1.tiers_idtiers = srvikey;
    -- DELETE FROM main.tiers_has_contact AS tb1 WHERE tb1.tiers_idtiers = srvikey; 

    IF EXISTS (SELECT 1 FROM main.payment WHERE payeur = srvikey or percepteur = srvikey) OR 
       EXISTS (SELECT 1 FROM main.commande WHERE client = srvikey or operateur = srvikey) THEN
       raise exception 'Cannot delete this client record, there is related orders or payments' using ERRCODE = 'TX712';
    ELSE
        SELECT tb1.identifiant INTO rolename FROM main.tiers AS tb1 WHERE tb1.idtiers = srvikey;

        IF rolename IS NOT NULL AND rolename <> ''::text THEN
            -- error_msg := 'DROP ROLE '::text || rolename;
            -- RAISE NOTICE '%', error_msg;
            EXECUTE 'DROP ROLE IF EXISTS '::text || rolename;  
        END IF; 

        DELETE FROM main.tiers AS tb1 WHERE tb1.idtiers = srvikey;
    END IF;
    
    RETURN query select error_msg;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;