﻿-- Function: gettiers()

DROP FUNCTION main.gettiers();

CREATE OR REPLACE FUNCTION main.gettiers()
  RETURNS TABLE(ikey integer, qtdisplayrole text, tnom text, tmodifprivileges text, tttiers text, tcivilite integer, tsex integer, topermaker integer, tprioritycomment integer, topercomment integer, topercomment2 text, tnomcol_qtdisplayrole text, tdatenaissancecol_qtdisplayrole timestamp without time zone, tcommentairecol_qtdisplayrole text, tidentifiantcol_qtdisplayrole text, tttierscol_qtdisplayrole text, tcivilitecol_qtdisplayrole text, taccountcol_qtdisplayrole real, tinfoentreprisecol_qtdisplayrole text, tdatecreationcol_qtdisplayrole timestamp without time zone, topermakercol_qtdisplayrole text, tprenomcol text) AS
$BODY$
BEGIN  /* MADE BY CYPC */   
  
    RETURN QUERY SELECT tb1.idtiers, 
                        tb1.prenom, 
                        tb1.nom,
                        tb1.modifprivileges,
                        upper(left(tb1.ptype::text, 1)) || right(tb1.ptype::text, length(tb1.ptype::text)-1), 
                        tb1.civilite_idcivilite,
                        tb1.sex,
                        tb4.idtiers,
                        tb1.commentpriority,
                        tb5.idtiers,
                        tb5.nom || ' '::text || tb5.prenom,
                        tb1.nom, 
                        tb1.datenaissance, 
                        tb1.commentaire, 
                        tb1.identifiant,  
                        upper(left(tb1.ptype::text, 1)) || right(tb1.ptype::text, length(tb1.ptype::text)-1), 
                        tb2.libelle, 
                        tb1.solde, 
                        CAST(tb1.info_entreprise AS text),
                        tb1.datecreation,
                        tb4.nom || ' '::text || tb4.prenom, 
                        tb1.prenom
                 FROM            main.tiers     AS tb1
                      INNER JOIN main.civilite  AS tb2 ON (tb2.idcivilite = tb1.civilite_idcivilite)
                      INNER JOIN main.tiers     AS tb4 ON (tb4.idtiers = tb1.operatormaker)
                      INNER JOIN main.tiers     AS tb5 ON (tb5.idtiers = tb1.operatorcomment) 
                 WHERE tb1.ptype = 'customer'
	         ORDER BY tb1.datecreation; 
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION main.gettiers()
  OWNER TO postgres;
