--Function: main.add1tocar(json)

--DROP FUNCTION "main".add1tocar(json);

CREATE OR REPLACE FUNCTION "main".add1tocar
(
  IN  params  json
)
RETURNS TABLE 
(
  ikey           integer,
  qtdisplayrole  text,
  dccoord        text,
  dcquantite     integer,
  pdescrip       text
)
AS
$$
DECLARE
    ztar integer;
    ccli text;
    n int;
    has_plan boolean;
BEGIN
    has_plan := (params->>'plan')::boolean;

    if has_plan then
        --Seleccion automatica de asientos.
        ztar := (params->>'ztar')::integer; if ztar is null then raise exception 'Invalid parameter <ztar>'; end if;
        ccli := (params->>'ccli')::text; if ccli is null then raise exception 'Invalid parameter <ccli>'; end if;
        n := (params->>'n')::int; if n is null then raise exception 'Invalid parameter <n>'; end if;
        --La version initial estaba implementada en el servidor debido a su complejidad: se trata de elegir asientos en un plano de sala, siguiento un algo de elecci�n  de asientos.

        raise exception 'Not yet implemented!';
    else
        return query select * from addnonlabeledprodtocart(params);
    end if; 
END
$$
LANGUAGE 'plpgsql'
COST 100;

ALTER FUNCTION "main".add1tocar(json)
  OWNER TO postgres;