--Function: main.addnonlabeledprodtocart(json)

--DROP FUNCTION "main".addnonlabeledprodtocart(json);

CREATE OR REPLACE FUNCTION "main".addnonlabeledprodtocart
(
  IN  params  json
)
RETURNS TABLE 
(
  ikey           integer,
  qtdisplayrole  text,
  dccoord        text,
  dcquantite     integer,
  pdescrip       text
)
AS
$$
declare
    idp integer;
    str text;
    m integer;
    si integer = -1;

    ztar integer;
    n integer;
    ccli text;
BEGIN
    ztar := (params->>'ztar')::integer; if ztar is null then raise exception 'Invalid parameter <ztar>'; end if;
    n := (params->>'n')::integer; if n is null then raise exception 'Invalid parameter <n>'; end if;
    ccli := (params->>'ccli')::text; if ccli is null then raise exception 'Invalid parameter <ccli>'; end if;
    
    si := main.getsessionid();
    if si = -1 then
       raise exception 'Session not found';
    end if;

    if not exists(select 1 from (main.ensemble e inner join main.zonetarif z on (e.idtarif = z.idzonetarif)) inner join main.grilletarif g on (z.idgrille = g.idtarif) where e.id = ztar and ccli = ANY(g.categoriesclient)) then
        raise exception 'This is not a valid tariff' using ERRCODE = 'TX706';
    end if;

    update main.stockproduit
    set quantite = case when totaldispo = -1
                        then quantite + n
                        else quantite + LEAST(n, GREATEST(0, totaldispo - quantite)) --<-- cantidad sin reservar, totaldispo = -1 si total no esta limitado
                   end
    where id in ( select s.id
                  from main.stockproduit s inner join main.productsinset p on (p.iditem = s.id)
                  where s.etat = 1 and p.idset = ztar and (s.totaldispo = -1 or s.quantite < s.totaldispo)
                  limit 1 --<--solo un producto por zona en el caso sin asientos
                )
    returning id,
              case when totaldispo = -1
                        then n
                        else LEAST(n, GREATEST(0, totaldispo - quantite))
              end,
              descrip
    into idp, m, str;

    if not found then
        raise exception 'Product not found' using ERRCODE = 'TX707';
    end if;

    update main.produitreserve
    set quant = quant + m
    where idsess = si and idprod = idp;
    
    if not found then
	--raise notice 'blocking record NOT found for product %', str;
	insert into main.produitreserve(idsess, idprod, quant)
	values(si, idp, m);
    end if;

    return query select idp, str, null::text, m, str;
END;
$$
LANGUAGE 'plpgsql'
COST 100;

ALTER FUNCTION "main".addnonlabeledprodtocart(json)
  OWNER TO postgres;