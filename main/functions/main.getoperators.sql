﻿DROP function main.getoperators();

CREATE FUNCTION main.getoperators() RETURNS TABLE(ikey integer, qtdisplayrole text, tnom text, tmodifprivileges text, tttiers main.persontype, tcivilite integer, tsex integer, topermaker integer, tprioritycomment integer, topercomment integer, topercomment2 text, tonomcol_qtdisplayrole text, todatenaissancecol_qtdisplayrole timestamp without time zone, tocommentairecol_qtdisplayrole text, toidentifiantcol_qtdisplayrole text, tottierscol_qtdisplayrole main.persontype, tocivilitecol_qtdisplayrole text, todatecreationcol_qtdisplayrole timestamp without time zone, toopermakercol_qtdisplayrole text)
    AS
$BODY$
BEGIN  /* MADE BY CYPC */   
  
     RETURN QUERY SELECT tb1.idtiers, 
                        tb1.prenom, 
                        tb1.nom,
                        tb1.modifprivileges,
                        tb1.ptype, 
                        tb1.civilite_idcivilite,
                        tb1.sex,
                        tb4.idtiers,
                        tb1.commentpriority,
                        tb5.idtiers,
                        tb5.nom || ' '::text || tb5.prenom,
                        tb1.nom, 
                        tb1.datenaissance, 
                        tb1.commentaire, 
                        tb1.identifiant, 
                        tb1.ptype,
                        tb2.libelle, 
                        tb1.datecreation,
                        tb4.nom || ' '::text || tb4.prenom
                 FROM            main.tiers     AS tb1
                      INNER JOIN main.civilite  AS tb2 ON (tb2.idcivilite = tb1.civilite_idcivilite)
                      INNER JOIN main.tiers     AS tb4 ON (tb4.idtiers = tb1.operatormaker)
                      INNER JOIN main.tiers     AS tb5 ON (tb5.idtiers = tb1.operatorcomment) 
                 WHERE tb1.ptype = 'cashier';
                                
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION main.getoperators()
  OWNER TO postgres;