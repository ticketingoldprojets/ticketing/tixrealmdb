﻿-- Function: inspartner(json)

-- DROP FUNCTION inspartner(json);

CREATE OR REPLACE FUNCTION main.inspartner(IN params json)
  RETURNS TABLE(errmsg text, ikey integer) AS
$BODY$
DECLARE
    error_msg    text    = ''::text;
    serial_key   integer = -1::integer;

    tnom text;
    tidentifiantcol_qtdisplayrole text;
    tpwd text;
    partneridsession text;
    partnerventedirecte boolean;
    cpartnerenvoyercontactes boolean;
    partneradressesrv text;
    partnerportsrv integer;
BEGIN  /* MADE BY CYPC */
    tnom := (params->>'tnom')::text; if tnom is null then raise exception 'Invalid parameter <tnom>'; end if;
    tidentifiantcol_qtdisplayrole := (params->>'tidentifiantcol_qtdisplayrole')::text; if tidentifiantcol_qtdisplayrole is null then raise exception 'Invalid parameter <tidentifiantcol_qtdisplayrole>'; end if;
    tpwd := (params->>'tpwd')::text; if tpwd is null then raise exception 'Invalid parameter <tpwd>'; end if;
    partneridsession := (params->>'partneridsession')::text; /*if partneridsession is null then raise exception 'Invalid parameter <partneridsession>'; end if;*/
    partnerventedirecte := (params->>'partnerventedirecte')::boolean; if partnerventedirecte is null then raise exception 'Invalid parameter <partnerventedirecte>'; end if;
    cpartnerenvoyercontactes := (params->>'cpartnerenvoyercontactes')::boolean; if cpartnerenvoyercontactes is null then raise exception 'Invalid parameter <cpartnerenvoyercontactes>'; end if;
    partneradressesrv := (params->>'partneradressesrv')::text; if partneradressesrv is null then raise exception 'Invalid parameter <partneradressesrv>'; end if;
    partnerportsrv := (params->>'partnerportsrv')::integer; if partnerportsrv is null then raise exception 'Invalid parameter <partnerportsrv>'; end if;
    
    INSERT INTO main.partenaire(nom, utilisateur, mdp, idsession, ventedirecte, envoyercontactes, adressesrv, portsrv, datecreation) 
	VALUES(tnom, tidentifiantcol_qtdisplayrole, tpwd, partneridsession, partnerventedirecte, cpartnerenvoyercontactes, partneradressesrv, partnerportsrv, LOCALTIMESTAMP) 
	RETURNING id INTO serial_key;

    RETURN QUERY SELECT error_msg, serial_key;
	
EXCEPTION

    WHEN NO_DATA_FOUND THEN 
        error_msg  := 'Error trying to insert the new record'::text;
        serial_key := -1::integer; 
        RETURN QUERY SELECT error_msg, serial_key;
 
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION main.inspartner(json)
  OWNER TO postgres;
