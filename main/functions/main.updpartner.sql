﻿-- Function: updpartner(json)

-- DROP FUNCTION updpartner(json);

CREATE OR REPLACE FUNCTION main.updpartner(
    IN params json,
    OUT errmsg text)
  RETURNS SETOF text AS
$BODY$
DECLARE
    error_msg    text    = ''::text;

    tnom text;
    tidentifiantcol_qtdisplayrole text;
    tpwd text;
    partneridsession text;
    partnerventedirecte boolean;
    cpartnerenvoyercontactes boolean;
    partneradressesrv text;
    partnerportsrv integer;
    p_id integer;
BEGIN  /* MADE BY CYPC */
	tnom := (params->>'tnom')::text; if tnom is null then raise exception 'Invalid parameter <tnom>'; end if;
	tidentifiantcol_qtdisplayrole := (params->>'tidentifiantcol_qtdisplayrole')::text; if tidentifiantcol_qtdisplayrole is null then raise exception 'Invalid parameter <tidentifiantcol_qtdisplayrole>'; end if;
	tpwd := (params->>'tpwd')::text; if tpwd is null then raise exception 'Invalid parameter <tpwd>'; end if;
	partneridsession := (params->>'partneridsession')::text; /*if partneridsession is null then raise exception 'Invalid parameter <partneridsession>'; end if;*/
	partnerventedirecte := (params->>'partnerventedirecte')::boolean; if partnerventedirecte is null then raise exception 'Invalid parameter <partnerventedirecte>'; end if;
	cpartnerenvoyercontactes := (params->>'cpartnerenvoyercontactes')::boolean; if cpartnerenvoyercontactes is null then raise exception 'Invalid parameter <cpartnerenvoyercontactes>'; end if;
	partneradressesrv := (params->>'partneradressesrv')::text; if partneradressesrv is null then raise exception 'Invalid parameter <partneradressesrv>'; end if;
	partnerportsrv := (params->>'partnerportsrv')::integer; if partnerportsrv is null then raise exception 'Invalid parameter <partnerportsrv>'; end if;
	p_id := (params->>'p_id')::integer; if p_id is null then raise exception 'Invalid parameter <p_id>'; end if;
	
	UPDATE main.partenaire AS tb1
        SET nom              = tnom, 
            utilisateur      = tidentifiantcol_qtdisplayrole, 
            mdp              = tpwd, 
            idsession        = partneridsession, 
            ventedirecte     = partnerventedirecte, 
            envoyercontactes = cpartnerenvoyercontactes, 
            adressesrv       = partneradressesrv,
            portsrv          = partnerportsrv
        WHERE tb1.id         = p_id;

    RETURN query select error_msg;
	
EXCEPTION

    WHEN NO_DATA_FOUND THEN 
        error_msg  := 'Error trying to update record'::text;
        RETURN query select error_msg;
 
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION main.updpartner(json)
  OWNER TO postgres;
